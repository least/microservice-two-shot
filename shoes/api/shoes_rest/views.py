from django.db.models import ObjectDoesNotExist
from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import Shoe, BinVO
import json

from common.json import ModelEncoder

class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties=[
        "import_href",
        "name"
    ]

class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties= [
        "id",
        "name",
        "manufacturer",
        # "color",
        "picture",
        "bin"
    ]
    # encoders={
    #     "bin": BinVODetailEncoder(),
    # }

    def get_extra_data(self, o):
        return {"bin": o.bin.name}

class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties= [
        "manufacturer",
        "name",
        "color",
        "picture",
        "id",
        "bin",
    ]
    encoders={
        "bin": BinVODetailEncoder(),
    }

# Create your views here.
@require_http_methods(["GET", "POST"])
def api_list_shoes(request, bin_vo_href=None):
    if request.method=="GET":
        if bin_vo_href is not None:
            print("bin id: ", bin_vo_href)
            shoes = Shoe.objects.filter(bin=bin_vo_href)
        else:
            shoes = Shoe.objects.all()
        print(shoes)
        return JsonResponse({"shoes": shoes}, encoder=ShoeListEncoder)
    else:
        content = json.loads(request.body)
        print(content)
        try:
            bin = BinVO.objects.get(import_href=content["bin"])
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse({"message": "Invalid Bin href"})
        shoe = Shoe.objects.create(**content)

        return JsonResponse(shoe, encoder=ShoeDetailEncoder, safe=False)


@require_http_methods(["PUT", "DELETE", "GET"])
def api_show_shoe(request, id):
    if request.method=="GET":
        shoe = Shoe.objects.get(id=id)
        return JsonResponse(shoe, encoder=ShoeDetailEncoder, safe=False)
    elif request.method=="PUT":
        content = json.loads(request.body)
        print(content)
    # try to get the bin
        try:
            if "bin" in content:
                bin = BinVO.objects.get(import_href=content["bin"])
                content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse( { "message": "Invalid Bin Id" })

        Shoe.objects.filter(id=id).update(**content)
        shoe = Shoe.objects.get(id=id)
        return JsonResponse(shoe, encoder=ShoeDetailEncoder, safe=False)
    else: # DELETE
        count, _ = Shoe.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})

    

