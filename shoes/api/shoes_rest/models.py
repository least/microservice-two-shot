from django.db import models

class BinVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name
    
class Shoe(models.Model):
    manufacturer = models.CharField(max_length=255)
    name = models.CharField(max_length=255, unique=True)
    color = models.CharField(max_length=255)
    picture = models.URLField(max_length=200)
    bin = models.ForeignKey(
        BinVO,
        related_name="shoes",
        on_delete=models.PROTECT,)

    def __str__(self):
        return f"{self.manufacturer} - {self.name}"

