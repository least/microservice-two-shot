
from django.urls import path, include

from shoes_rest.views import api_list_shoes,api_show_shoe

urlpatterns = [
    path("shoes/", api_list_shoes, name="api_list_shoes"),
    path("bin/<int:bin_vo_href>/shoes/", api_list_shoes, name="api_list_shoes"),
    path("shoes/<int:id>/", api_show_shoe, name="api_show_shoe"),
]


