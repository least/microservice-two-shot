import { BrowserRouter, Routes, Route } from "react-router-dom";
import { useState, useEffect } from "react";
import MainPage from "./MainPage";
import Nav from "./Nav";
import Shoes from "./Shoes";
import NewShoe from "./Shoes/NewShoe";
import ShoeDetail from "./Shoes/ShoeDetail";
import ListHats from "./hats/ListHats";
import CreateHat from "./hats/CreateHat";

function App() {
  const [shoe, setShoe] = useState({});
  const [shoeFound, setShoeFound] = useState(false);
  const [hats, setHats] = useState([]);
  async function loadHats() {
    const response = await fetch("http://localhost:8090/api/hats/");
    if (response.ok) {
      const data = await response.json();
      setHats(data.hats);
    } else {
      // console.error(response);
    }
  }
  const fetchShoe = async (shoeId) => {
    try {
      const url = `http://localhost:8080/api/shoes/${shoeId}/`;
      const res = await fetch(url);
      if (res.ok) {
        const data = await res.json();
        setShoe(data);
        setShoeFound(true);
      }
    } catch (e) {
      // console.error(e)
    }
  };
  useEffect(() => {
    loadHats();
  }, []);
  return (
    <BrowserRouter>
      <Nav />
      <div className="container mb-5">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/hats">
            <Route
              index
              element={<ListHats hats={hats} loadHats={loadHats} />}
            />
            <Route path="new" element={<CreateHat loadHats={loadHats} />} />
          </Route>
          <Route path="/shoes">
            <Route index element={<Shoes />} />
            <Route
              path=":shoeId"
              element={
                <ShoeDetail
                  fetchShoe={fetchShoe}
                  shoeFound={shoeFound}
                  shoe={shoe}
                />
              }
            />
            <Route
              path=":shoeId/edit"
              element={
                <NewShoe
                  fetchShoe={fetchShoe}
                  shoeFound={shoeFound}
                  shoe={shoe}
                  setShoe={setShoe}
                />
              }
            />
            <Route path="new" element={<NewShoe />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
