import { useState, useEffect, useRef } from "react";
import { useNavigate, useParams } from "react-router-dom";
export default function NewShoe({ fetchShoe, shoeFound, shoe, setShoe }) {
  const [formData, setFormData] = useState({});
  const [originalData, setOriginalData] = useState({});
  const [bins, setBins] = useState([]);
  const formRef = useRef(null);

  const [editView, setEditView] = useState(false);

  useEffect(() => {
    if (shoeId) {
      setEditView(true);
      fetchShoe(shoeId);
    } else {
      setEditView(false);
      setFormData({});
    }
  }, [editView]);

  useEffect(() => {
    if (shoe && shoe.bin) {
      let data = {
        name: shoe.name,
        manufacturer: shoe.manufacturer,
        color: shoe.color,
        picture: shoe.picture,
        bin: shoe.bin.import_href,
      };
      setFormData(data);
      setOriginalData(data);
    }
  }, [shoeFound]);

  const { shoeId } = useParams();
  const [status, setStatus] = useState("pending");
  const [statusMessage, setStatusMessage] = "";
  const navigate = useNavigate();

  const fetchBins = async () => {
    const url = "http://localhost:8100/api/bins";
    const res = await fetch(url);
    const data = await res.json();
    setBins(data.bins);
  };

  // const fetchShoe = async () => {};

  const handleSubmit = async (e) => {
    e.preventDefault();

    if (!editView) {
      try {
        const addShoeUrl = "http://localhost:8080/api/shoes/";
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(formData),
          headers: {
            "Content-Type": "application/json",
          },
        };
        const res = await fetch(addShoeUrl, fetchConfig);
        if (res.ok) {
          const newShoe = await res.json();

          setFormData([]);
          setStatus("success");
          formRef.current.reset();
          navigate(`/shoes/${newShoe.id}`);
        } else {
          setStatus("failed");
          setStatusMessage("Could not create new shoe.");
        }
      } catch (e) {
        // console.error(e)
      }
    } else {
      const editShoeUrl = `http://localhost:8080/api/shoes/${shoeId}/`;
      const fetchConfig = {
        method: "PUT",
        body: JSON.stringify(formData),
        headers: {
          "Content-Type": "application/json",
        },
      };
      const res = await fetch(editShoeUrl, fetchConfig);
      if (res.ok) {
        const editedShoe = await res.json();
        setShoe(editedShoe);
        setOriginalData(formData);
        setStatus("success");
      } else {
        setStatus("failed");
      }
    }
  };

  useEffect(() => {
    if (status !== "pending")
      setTimeout(() => {
        setStatus("pending");
        setStatusMessage("");
      }, 3000);
  }, [status]);
  useEffect(() => {
    if (editView) {
      fetchShoe(shoeId);
    }

    fetchBins();
  }, []);

  const editResetForm = (e) => {
    e.preventDefault();
    setFormData(originalData);
  };

  const handleChange = (e) => {
    setFormData({
      ...formData,
      [e.target.name]: e.target.value,
    });
  };

  return (
    <div className="row">
      <div className="col-sm-10 offset-sm-1 offset-md-3 col-md-6 ">
        <div className="shadow mt-4 p-4">
          <h1 className="text-muted mb-4">
            {editView ? `Edit ${shoeFound ? shoe.name : "shoe"}` : "Add Shoe"}
          </h1>
          {editView ? (
            <img
              className="img-fluid rounded mb-4"
              src={shoe.picture}
              alt={shoe.name}
            />
          ) : (
            ""
          )}
          <form id="create-shoe-form" ref={formRef} onSubmit={handleSubmit}>
            {status === "success" && (
              <div className="alert alert-success" role="alert">
                Success! Edited {shoe.name}
              </div>
            )}
            {status === "failed" && (
              <div className="alert alert-danger" role="alert">
                Something went wrong. {statusMessage}
              </div>
            )}
            <div className="form-floating mb-3">
              <input
                className="form-control"
                name="name"
                placeholder="name"
                type="text"
                value={formData.name ?? ""}
                onChange={handleChange}
              />
              <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                className="form-control"
                placeholder="manufacturer"
                name="manufacturer"
                type="text"
                value={formData.manufacturer ?? ""}
                onChange={handleChange}
              />
              <label htmlFor="manufacturer">Manufacturer</label>
            </div>
            <div className="form-floating mb-3">
              <input
                className="form-control"
                placeholder="color"
                name="color"
                type="text"
                value={formData.color ?? ""}
                onChange={handleChange}
              />
              <label htmlFor="color">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input
                className="form-control"
                placeholder="picture"
                name="picture"
                type="text"
                value={formData.picture ?? ""}
                onChange={handleChange}
              />
              <label htmlFor="picture">Picture URL</label>
            </div>
            <div className="form-floating mb-3">
              <select
                name="bin"
                id="bin-select"
                className="form-select"
                value={formData.bin ?? ""}
                onChange={handleChange}
              >
                <option defaultValue value="">
                  Choose a Bin
                </option>
                {bins?.map((bin) => {
                  return (
                    <option key={bin.href} name="bin" value={bin.href}>
                      {bin.closet_name}
                    </option>
                  );
                })}
              </select>
              <label htmlFor="bin">Bin</label>
            </div>

            <div className="d-flex justify-content-between">
              <button className="btn btn-outline-secondary">Submit</button>
              {editView ? (
                <button className="btn btn-danger" onClick={editResetForm}>
                  Edit Reset
                </button>
              ) : (
                ""
              )}
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}
