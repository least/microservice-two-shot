import { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
export default function ShoeDetail({ fetchShoe, shoe, shoeFound }) {
  const [bin, setBin] = useState({});
  const { shoeId } = useParams();

  const fetchBin = async () => {
    try {
      const url = `http://localhost:8100${shoe.bin.import_href}`;
      const res = await fetch(url);
      const data = await res.json();
      setBin(data);
    } catch (e) {
      // console.error(e)
    }
  };

  useEffect(() => {
    if (shoe && shoe.bin) {
      fetchBin();
    }
  }, [shoe]);

  useEffect(() => {
    fetchShoe(shoeId);
  }, []);

  if (shoeFound)
    return (
      <div className="row mt-4">
        <h4>{shoe.name}</h4>
        <img src={shoe.picture} alt={shoe.name} className="col-lg-4" />
        <div className="col-lg-8">
          <h5 className="text-muted">{shoe.manufacturer}</h5>
          <dl className="row">
            <dt className="col-3">Color:</dt>
            <dd className="col-9">{shoe.color}</dd>
            <dt className="col-3">Closet Name:</dt>
            <dd className="col-9">{bin.closet_name}</dd>
            <dt className="col-3">Bin Number: </dt>
            <dd className="col-9">{bin.bin_number}</dd>
            <dt className="col-3">Bin Size: </dt>
            <dd className="col-9">{bin.bin_size}</dd>
          </dl>
          <Link to="edit" className="btn btn-outline-secondary">
            Edit Shoe
          </Link>
        </div>
      </div>
    );
  else {
    return <h1 className="mt-5 text-muted">Shoe Not Found</h1>;
  }
}
