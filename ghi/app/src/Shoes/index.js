import { useEffect, useState } from "react";
import { Link } from "react-router-dom";

const DeleteModal = ({ name, toggleDeleteModal, deleteShoe }) => {
  return (
    <div
      className="modal"
      style={{ display: "block", background: "rgba(0,0,0,0.6)" }}
      onClick={toggleDeleteModal}
    >
      <div className="modal-dialog modal-dialog-centered">
        <div className="modal-content">
          <div className="modal-header">
            <h1 className="modal-title fs-5" id="exampleModalCenterTitle">
              Delete Shoe
            </h1>
            <button
              type="button"
              className="btn-close"
              data-bs-dismiss="modal"
              aria-label="Close"
              onClick={toggleDeleteModal}
            ></button>
          </div>
          <div className="modal-body">
            <p>
              Are you sure you want to delete the shoe, "<em>{name}</em>"? This
              action cannot be undone.
            </p>
          </div>
          <div className="modal-footer">
            <button
              type="button"
              className="btn btn-danger"
              onClick={deleteShoe}
            >
              Yes, delete permanently.
            </button>
            <button
              type="button"
              className="btn btn-secondary"
              data-bs-dismiss="modal"
              onClick={toggleDeleteModal}
            >
              Cancel
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

const ShoeCard = ({ manufacturer, name, picture, id, fetchShoes }) => {
  const [showModal, setShowModal] = useState(false);

  const toggleDeleteModal = () => {
    setShowModal(!showModal);
  };

  const deleteShoe = async () => {
    try {
      const url = `http://localhost:8080/api/shoes/${id}`;
      const fetchConfig = {
        method: "DELETE",
      };
      const res = await fetch(url, fetchConfig);
      if (res.ok) {
        const data = await res.json();
        fetchShoes();
      } else {
        console.error("could not delete shoe");
      }
    } catch (e) {
      console.error(e);
    }
  };

  return (
    <div className="col-xs-12 col-sm-6 col-md-6 col-lg-4 mt-4">
      <div className="card shadow h-100">
        {showModal ? (
          <DeleteModal
            name={name}
            toggleDeleteModal={toggleDeleteModal}
            deleteShoe={deleteShoe}
          />
        ) : null}
        <div className="ratio ratio-1x1">
          <img
            className="card-img-top"
            src={picture}
            style={{ objectFit: "cover" }}
            alt={name}
          />
        </div>
        <div className="card-body">
          <h5 className="title">{name}</h5>
          <h6 className="subtitle text-muted mb-5">{manufacturer}</h6>
        </div>
        <div className="card-footer d-flex justify-content-between flex-sm-column flex-md-row">
          <Link
            className="btn btn-outline-secondary bt-3 mb-2"
            to={`/shoes/${id}`}
          >
            Shoe Details
          </Link>
          <button
            className="btn btn-outline-danger bt-3 mb-2"
            onClick={toggleDeleteModal}
          >
            Delete Shoe
          </button>
        </div>
      </div>
    </div>
  );
};
export default function Shoes() {
  const [shoes, setShoes] = useState([]);

  const fetchShoes = async () => {
    try {
      const url = "http://localhost:8080/api/shoes/";
      const res = await fetch(url);
      if (res.ok) {
        const data = await res.json();
        setShoes(data.shoes);
      }
    } catch (e) {
      console.error(e);
    }
  };

  useEffect(() => {
    fetchShoes();
  }, []);

  return (
    <div>
      <h1 className="mt-4">Shoes</h1>
      <Link to="new" className="btn btn-outline-secondary">
        Add a Shoe
      </Link>
      <div className="row">
        {shoes.map((shoe) => {
          return (
            <ShoeCard
              key={shoe.id + shoe.name}
              manufacturer={shoe.manufacturer}
              name={shoe.name}
              picture={shoe.picture}
              id={shoe?.id}
              fetchShoes={fetchShoes}
            />
          );
        })}
      </div>
    </div>
  );
}
