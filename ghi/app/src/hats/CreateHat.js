import { useState, useEffect } from "react";








function CreateHat({loadHats}){
    const [locations, setLocations] = useState([]);
    const [fabric, setFabric] = useState("");
    const [styleName, setStyleName] = useState("");
    const [color, setColor] = useState("");
    const [pictureUrl, setPictureUrl] = useState("");
    const [loc, setLoc] = useState("");

    function handleFabricChange(event){
        setFabric(event.target.value)
    }
    function handleStyleName(event){
        setStyleName(event.target.value)
    }
    function handleColor(event){
        setColor(event.target.value)
    }
    function handlePictureUrl(event){
        setPictureUrl(event.target.value)
    }
    function handleLocation(event){
        setLoc(event.target.value)
    }

    async function loadLocations() {
      const response = await fetch("http://localhost:8100/api/locations/");
      if (response.ok) {
        const data = await response.json();
        setLocations(data.locations);
      } else {
        console.error(response);
      }
    }
    useEffect(() => {
      loadLocations();
    }, []);
    async function handleSubmit(event) {
        event.preventDefault()
        const data={}
    //    {
    //        "fabric": "cloth",
    //        "style_name": "classy",
    //        "color": "red",
    //        "picture": "https://img.kwcdn.com/product/Fancyalgo/VirtualModelMatting/25e6e28df46819b48e263da1b17a45a0.jpg?imageView2/2/w/800/q/70/format/webp",
    //        "location": 5
    //    }
    data.fabric=fabric
    data.style_name=styleName
    data.color=color
    data.picture=pictureUrl
    data.location=loc
    const url="http://localhost:8090/api/hats/"
    const fetchConfig = {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
          'Content-Type': 'application/json'
        }
      }
      const res = await fetch(url, fetchConfig)
      
      if (res.ok){
        const newHat=await res.json()
     loadHats()
     setFabric("")
     setStyleName("")
     setColor("")
     setPictureUrl("")
     setLoc("")
      }
    }
    return <div className="row">
    <div className="col-sm-10 offset-sm-1 offset-md-3 col-md-6 ">
      <div className="shadow mt-4 p-4">
        <h1 className="text-muted mb-4">CreateHat</h1>

        <form id="create-shoe-form"  onSubmit={handleSubmit}>

          <div className="form-floating mb-3">
            <input className="form-control" name="fabric" placeholder='fabric' type="text" value={fabric} onChange={handleFabricChange} />
            <label htmlFor="fabric">fabric</label>
          </div>
          <div className="form-floating mb-3">
            <input className="form-control" placeholder='style name' name="style_name" type="text" value={styleName} onChange={handleStyleName}/>
            <label htmlFor="style_name">style name</label>
          </div>
          <div className="form-floating mb-3">
            <input className="form-control" placeholder='color' name="color" type="text"  value={color} onChange={handleColor}/>
            <label htmlFor="color">Color</label>
          </div>
          <div className="form-floating mb-3">
            <input className="form-control" placeholder='picture' name="picture" type="text" value={pictureUrl} onChange={handlePictureUrl} />
            <label htmlFor="picture">Picture URL</label>
          </div>
          <div className="form-floating mb-3">
            <select name="location" id="location-select" className="form-select" value={loc} onChange={handleLocation}>
              <option defaultValue value="">Choose a location</option>
              {locations?.map(location => {
                return <option key={location.id} value={location.href}>{location.closet_name}</option>
              } ) }

            </select>
            <label htmlFor="location">location</label>
          </div>


          <div className="d-flex justify-content-between">
            <button className="btn btn-outline-secondary">Submit</button>

          </div>
        </form>
      </div>
    </div >
  </div >
}


export default CreateHat
