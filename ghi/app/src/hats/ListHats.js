import { useState, useEffect } from "react";
import { Link } from "react-router-dom"

function ListHats({hats,loadHats}) {

async function handleDelete(hatId){
    const url=`http://localhost:8090/api/hats/${hatId}/`
    const fetchConfig = {
        method: "DELETE",
      }
    const res=await fetch(url,fetchConfig)
    if (res.ok){
        loadHats()
    }
}

  return (
    <div>
          <Link to='new' className="btn btn-outline-secondary">Add a Hat</Link>
      <table>
        
        <thead>
          <tr>
            <th>fabric</th>
            <th>style_name</th>
            <th>color</th>
            <th>picture</th>
            <th>location</th>
          </tr>
        </thead>
        <tbody>
          {hats.map((hat) => {
            return (
              <tr key={hat.id}>
                <td>{hat.fabric}</td>
                <td>{hat.style_name}</td>
                <td>{hat.color}</td>
                <td>
                  <img
                    width="200"
                    height="200"
                    className="card-img-top"
                    src={hat.picture}
                  />
                </td>
                <td>{hat.location.name}</td>
                <td><button onClick={() => handleDelete(hat.id)}>delete</button></td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default ListHats;
