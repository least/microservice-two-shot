# Wardrobify

Team:

* Harold - Hats
* Josh - Shoes

## Design

## Shoes microservice

The Shoes microservice allows a user to store and retrieve shoes 
and their location  by bin.

### Models

The shoes microservice uses two models to accomplish its purpose,
a `Shoe` and a `BinVO`. The `BinVO` object being a value object
pointing back to the `Bin` object in out wardrobe microservice. 

#### Shoe

**manufacturer**: The manufacturer is a short string field

**name**: The name is a short string field of model name of the shoe.
      Uniqueness is enforced.

**color**: The color is a short string field of the color of the shoe.

**picture**: The picture is an url field, linking to an image of the shoe 

**bin**: The bin is a foreign key relationship to the binVO, which allows us
     to reference the bin object in the Wardrobe microservice. 

#### BinVO

**import_href**: an url field referencing back to the Bin item on the
             Wardrobe microservice

**name**: The name is a short string field of the name of the Bin


### Wardrobe Microservice Integration

Integration with the Wardrobe microservice was accomplished using 
a poller, that requested a list of all bins from the Wardrobe 
microservice at a 60 second interval. 

When received, it creates or updates the BinVO items on the Shoes
microservice.


## Hats microservice

worked on the models so we a place for the data

worked on the views so we can pass the information to the
front end

worked on the urls to give the project control of
the information

we worked on javascript to perform the polling for
information

we tested all the parts slowly until everything worked
together as expected

and bob your uncle!
