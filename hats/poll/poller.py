import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "hats_project.settings")
django.setup()


# def get_hats():                                                                        # just added
#     response = requests.get("http://monolith:8000/api/wardrobe")                       # just added
#     content = json.loads(response.content)                                             # just added
#     for hats in content["hats"]:                                                       # just added
#         hatsVO.object.update_or_create(                                                # just added
#             import_href=hats["href"],                                                  # just added
#             defaults={"fabric": hats["fabric"],                                        # just added
#                       "style_name": hats["style_name"],                                # just added
#                       "color": hats["color"],                                          # just added
#                       "picture": hats["picture"],                                      # just added
#                       "location_in_wardrobe": hats["location_in_wardrobe"]},           # just added
#         )                                                                              # just added

from hats_rest.models import LocationVO

def get_location():                                                                     
    response = requests.get("http://wardrobe-api:8000/api/locations")                       
    content = json.loads(response.content)                                             
    for location in content["locations"]:                                                
        LocationVO.objects.update_or_create(                                                
            import_href=location["href"],                                                  
            defaults={"name": location["closet_name"]})                                                                             # just added

def poll():
    while True:
        print('Hats poller polling for data')
        try:
            get_location()                                                                 
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
