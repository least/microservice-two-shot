from django.db import models


class LocationVO(models.Model):
    import_href=models.CharField(max_length=200, unique=True)
    name=models.CharField(max_length=200)

    def __str__(self):
        return self.name


class Hat(models.Model):
    fabric = models.CharField(max_length=50)
    style_name = models.CharField(max_length=50)
    color = models.CharField(max_length=50)
    picture = models.URLField()
    location = models.ForeignKey(
        LocationVO,
        related_name="hats",
        on_delete=models.CASCADE,
    )

    def __str__self():
        return self.name
    
    class Meta:
        ordering = ("style_name",)
