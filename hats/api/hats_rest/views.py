from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from django.shortcuts import render
from .models import Hat, LocationVO
from common.json import ModelEncoder
import json


class LocationVODetailEncoder(ModelEncoder):
    model=LocationVO
    properties=["name","import_href","id"]


class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "id",
        "fabric",
        "style_name",
        "color",
        "picture",
        "location",
    ]
    encoders = {
        "location": LocationVODetailEncoder(),
    }


@require_http_methods(["GET","POST"])
def view_hats(request):
    if request.method=="GET":
        hats=Hat.objects.all() # get all hats
        return JsonResponse(
            {"hats":hats},
            encoder=HatDetailEncoder,
        )
    elif request.method=="POST":
        content=json.loads(request.body)
        print(content)
        try:
            location=LocationVO.objects.get(import_href=content["location"])
            print(location)
            content["location"]=location
        except LocationVO.DoesNotExist:
            return JsonResponse(
            {"message": "Invalid location id"})

        hats=Hat.objects.create(**content)
        return JsonResponse(
            hats,
            encoder=HatDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE","GET","PUT"])
def view_hat(request,id):
    if request.method=="GET":
        hats=Hat.objects.get(id)
        return JsonResponse(
            hats,
            encoder=HatDetailEncoder,        
        )
    elif request.method=="DELETE":
        count,_=Hat.objects.filter(id=id).delete()
        return JsonResponse({"deleted":count>0})
    else:
       try:
          if "location" in content:
              location=LocationVO.objects.get(import_href=content["location"])
              content["location"]=location
       except LocationVO.DoesNotExist:
           return JsonResponse({"message": "Invalid location"})
            
       hats.objects.filter(id=id).update(**content)
       return JsonResponse(
                location,
                encoder=HatDetailEncoder,
                safe=False,
       )