from django.urls import path
from .views import view_hats, view_hat 

urlpatterns=[
    path("hats/",view_hats,name="view_hats"),
    path("hats/<int:id>/",view_hat,name="view_hat"),
]